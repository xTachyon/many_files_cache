#include "data.hpp"

namespace format {
    File::File(const wstring& name, uint32_t offset, uint32_t size)
        : name(name), offset(offset), size(size) {}
    
    File File::deserialize(istream& stream) {
        uint32_t string_size;
        QUICK_READ(string_size);
        wstring name(string_size, 0);
        stream.read(reinterpret_cast<char*>(name.data()), string_size * 2);

        uint32_t offset;
        QUICK_READ(offset);
        uint32_t size;
        QUICK_READ(size);

        return { name, offset, size };
    }

    void File::serialize(ostream& stream) const {
        uint32_t size = name.size();
        QUICK_WRITE(size);
        stream.write(reinterpret_cast<const char*>(name.c_str()), name.size() * sizeof(wchar_t));

        QUICK_WRITE(offset);
        QUICK_WRITE(this->size);
    }
}