#pragma once

#include <istream>
#include <ostream>
#include <string>

using std::istream;
using std::ostream;
using std::wstring;

#define QUICK_READ(var) stream.read(reinterpret_cast<char*>(&var), sizeof(var));
#define QUICK_WRITE(var) stream.write(reinterpret_cast<const char*>(&var), sizeof(var)); stream.flush();

namespace format {
    struct File {
        wstring name;
        uint32_t offset;
        uint32_t size;

        File(const wstring& name, uint32_t offset, uint32_t size);

        static File deserialize(istream& stream);
        void serialize(ostream& stream) const;
    };
}