#include <filesystem>
#include <fstream>
#include <iostream>
#include <vector>
#include "data.hpp"

using std::wcout;
using std::ifstream;
using std::ofstream;
using std::sort;
using std::vector;
using std::wstring;

namespace fs = std::experimental::filesystem;

void get_all_files(const fs::path& start, vector<wstring>& output) {
    for (auto& file : fs::directory_iterator(start)) {
        if (fs::is_regular_file(file)) {
            auto absolute = fs::absolute(file.path());
            output.push_back(absolute.wstring());
        } else {
            get_all_files(file, output);
        }
    }
}

void write_one_file(istream& input, ostream& output, uint32_t file_size) {
    vector<char> buffer(file_size + 1);
    while (true) {
        input.read(buffer.data(), buffer.size());
        auto read = input.gcount();
        if (read == 0) {
            break;
        }
        output.write(buffer.data(), read);
    }
}

void write_big_file(ostream& stream, const vector<wstring>& files, size_t original_folder_size) {
    vector<format::File> files_table;
    uint32_t offset = 0;

    size_t counter = 0;
    for (auto& file : files) {
        wcout << counter++ << L'/' << files.size() << " - " << file << L'\n';

        ifstream current(file, ifstream::binary);
        auto file_size = static_cast<uint32_t>(fs::file_size(file));
        write_one_file(current, stream, file_size);

        files_table.emplace_back(file.c_str() + original_folder_size, offset, file_size);
        offset += file_size;
    }
    //offset = stream.tellp();

    uint32_t size = files.size();
    QUICK_WRITE(size);

    for (auto& entry : files_table) {
        entry.serialize(stream);
    }

    QUICK_WRITE(offset);
}

int main(int argc, char** argv) {
    auto folder = fs::absolute(argv[1]);
    auto folder_string = folder.wstring();

    vector<wstring> files;
    get_all_files(argv[1], files);
    sort(files.begin(), files.end());

    ofstream big_file("big_file", ofstream::binary);
    write_big_file(big_file, files, folder_string.size() + 1);
}