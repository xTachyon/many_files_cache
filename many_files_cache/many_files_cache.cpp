#define WIN32_LEAN_AND_MEAN
#include <Winsock2.h>
#include <windows.h>
#include <detours.h>
#include <cstdint>
#include <fstream>
#include <map>
#include <string>
#include <experimental/filesystem>
#include <algorithm>
#include <cctype>
#include "data.hpp"

namespace fs = std::experimental::filesystem;

using std::map;
using std::ifstream;
using std::transform;
using std::vector;
using std::wofstream;
using std::wstring;

using CreateFileWType = decltype(CreateFileW);
using ReadFileType = decltype(ReadFile);
using CloseHandleType = decltype(CloseHandle);
using GetFileSizeType = decltype(GetFileSize);
using SetFilePointerType = decltype(SetFilePointer);
using GetFileTypeType = decltype(GetFileType);
using LoadLibraryWType = decltype(LoadLibraryW);
using CreateProcessWType = decltype(CreateProcessW);

const HANDLE START_HANDLE_ADDRESS = reinterpret_cast<HANDLE>(0xFFFF0000);

HANDLE make_handle(uint32_t position) {
    auto handle = static_cast<char*>(START_HANDLE_ADDRESS);
    handle += position;
    return handle;
}

uint32_t get_handle_position(HANDLE handle) {
    return static_cast<char*>(handle) - static_cast<char*>(START_HANDLE_ADDRESS);
}

void to_lower_inplace(wstring& string) {
    transform(string.begin(), string.end(), string.begin(), [](auto x) {
        return tolower(x);
        });
}

wstring to_lower(const wstring& string) {
    wstring copy(string);
    to_lower_inplace(copy);
    return copy;
}

struct HandleInfo {
    format::File* file;
    uint32_t offset;

    HandleInfo(format::File* file = nullptr, uint32_t offset = 0);
};

HandleInfo::HandleInfo(format::File* file, uint32_t offset)
    : file(file), offset(offset) {}


struct HookerStruct {
    ifstream big_file;
    wofstream output;
    map<wstring, format::File> files;
    wstring current_directory;

    vector<HandleInfo> handles;
    vector<uint32_t> free_positions;

    CreateFileWType* real_CreateFileW;
    ReadFileType* real_ReadFile;
    CloseHandleType* real_CloseHandle;
    GetFileSizeType* real_GetFileSize;
    SetFilePointerType* real_SetFilePointer;
    GetFileTypeType* real_GetFileType;
    LoadLibraryWType* real_LoadLibraryW;
    CreateProcessWType* real_CreateProcessW;

    char buffer[16 * 1024 * 1024];

    void on_load();
    void destroy();

    HookerStruct();

    uint32_t get_new_handle_position();
    void close_handle(HANDLE handle);
    uint32_t read_file(uint32_t handle, char* buffer, size_t to_read);
    uint32_t get_file_size(uint32_t handle);
    uint32_t set_file_pointer(uint32_t handle, int64_t move, uint32_t how_to_move);
};

HookerStruct hooker_struct;

#define CALL_REAL_CREATEFILEW hooker_struct.real_CreateFileW(lpFileName, dwDesiredAccess, dwShareMode,\
            lpSecurityAttributes, dwCreationDisposition, dwFlagsAndAttributes, hTemplateFile)

HANDLE WINAPI hooked_CreateFileW(
    LPCWSTR lpFileName,
    DWORD dwDesiredAccess,
    DWORD dwShareMode,
    LPSECURITY_ATTRIBUTES lpSecurityAttributes,
    DWORD dwCreationDisposition,
    DWORD dwFlagsAndAttributes,
    HANDLE hTemplateFile
) {
    if (dwDesiredAccess != GENERIC_READ) {
        return CALL_REAL_CREATEFILEW;
    }
    auto absolute = fs::canonical(lpFileName).wstring();
    to_lower_inplace(absolute);
    if (!absolute._Starts_with(hooker_struct.current_directory)) {
        return CALL_REAL_CREATEFILEW;
    }

    auto it = hooker_struct.files.find(absolute.c_str() + hooker_struct.current_directory.size() + 1);
    if (it == hooker_struct.files.end()) {
        return CALL_REAL_CREATEFILEW;
    }

    auto handle_position = hooker_struct.get_new_handle_position();
    auto& handle = hooker_struct.handles[handle_position];
    handle.file = &it->second;
    handle.offset = 0;

    return make_handle(handle_position);
}

BOOL WINAPI hooked_ReadFile(HANDLE hFile,
    LPVOID lpBuffer,
    DWORD nNumberOfBytesToRead,
    LPDWORD lpNumberOfBytesRead,
    LPOVERLAPPED lpOverlapped
) {
    if (hFile >= START_HANDLE_ADDRESS) {
        auto read = hooker_struct.read_file(get_handle_position(hFile), static_cast<char*>(lpBuffer), nNumberOfBytesToRead);
        if (lpNumberOfBytesRead) {
            *lpNumberOfBytesRead = read;
        }
        return true;
    }
    return hooker_struct.real_ReadFile(hFile, lpBuffer, nNumberOfBytesToRead, lpNumberOfBytesRead, lpOverlapped);
}

BOOL WINAPI hooked_CloseHandle(HANDLE hObject) {
    if (hObject >= START_HANDLE_ADDRESS) {
        hooker_struct.close_handle(hObject);
        return true;
    }
    return hooker_struct.real_CloseHandle(hObject);
}

DWORD WINAPI hooked_GetFileSize(HANDLE hFile, LPDWORD lpFileSizeHigh) {
    if (hFile >= START_HANDLE_ADDRESS) {
        auto size = hooker_struct.get_file_size(get_handle_position(hFile));
        if (lpFileSizeHigh) {
            *lpFileSizeHigh = 0;
        }
        return size;
    }
    return hooker_struct.real_GetFileSize(hFile, lpFileSizeHigh);
}

DWORD WINAPI hooked_SetFilePointer(
    HANDLE hFile,
    LONG lDistanceToMove,
    PLONG lpDistanceToMoveHigh,
    DWORD dwMoveMethod
) {
    if (hFile >= START_HANDLE_ADDRESS) {
        int64_t move = lDistanceToMove;
        if (lpDistanceToMoveHigh) {
            move |= static_cast<int64_t>(*lpDistanceToMoveHigh) << 32;
        }
        return hooker_struct.set_file_pointer(get_handle_position(hFile), move, dwMoveMethod);
    }
    return hooker_struct.real_SetFilePointer(hFile, lDistanceToMove, lpDistanceToMoveHigh, dwMoveMethod);
}

DWORD WINAPI hooked_GetFileType(HANDLE hFile) {
    if (hFile >= START_HANDLE_ADDRESS) {
        return FILE_TYPE_DISK;
    }
    return hooker_struct.real_GetFileType(hFile);
}

HMODULE WINAPI hooked_LoadLibraryW(LPCWSTR lpLibFileName) {
    /*hooker_struct.output << lpLibFileName << '\n';
    hooker_struct.output.flush();*/
    return hooker_struct.real_LoadLibraryW(lpLibFileName);
}

BOOL WINAPI hooked_CreateProcessW(
    LPCWSTR lpApplicationName,
    LPWSTR lpCommandLine,
    LPSECURITY_ATTRIBUTES lpProcessAttributes,
    LPSECURITY_ATTRIBUTES lpThreadAttributes,
    BOOL bInheritHandles,
    DWORD dwCreationFlags,
    LPVOID lpEnvironment,
    LPCWSTR lpCurrentDirectory,
    LPSTARTUPINFOW lpStartupInfo,
    LPPROCESS_INFORMATION lpProcessInformation
) {
    return hooker_struct.real_CreateProcessW(lpApplicationName, lpCommandLine, lpProcessAttributes, lpThreadAttributes,
        bInheritHandles, dwCreationFlags, lpEnvironment, lpCurrentDirectory, lpStartupInfo, lpProcessInformation);
}

void HookerStruct::on_load() {
    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
   /* DetourAttach(&(PVOID&)real_CreateFileW, hooked_CreateFileW);
    DetourAttach(&(PVOID&)real_ReadFile, hooked_ReadFile);
    DetourAttach(&(PVOID&)real_CloseHandle, hooked_CloseHandle);
    DetourAttach(&(PVOID&)real_GetFileSize, hooked_GetFileSize);
    DetourAttach(&(PVOID&)real_SetFilePointer, hooked_SetFilePointer);
    DetourAttach(&(PVOID&)real_GetFileType, hooked_GetFileType);*/
    DetourAttach(&(PVOID&)real_LoadLibraryW, hooked_LoadLibraryW);
    DetourAttach(&(PVOID&)real_CreateProcessW, hooked_CreateProcessW);
    DetourTransactionCommit();
}

void HookerStruct::destroy() {
    output.flush();
}

HookerStruct::HookerStruct() {
    real_CreateFileW = CreateFileW;
    real_ReadFile = ReadFile;
    real_CloseHandle = CloseHandle;
    real_GetFileSize = GetFileSize;
    real_SetFilePointer = SetFilePointer;
    real_GetFileType = GetFileType;
    real_LoadLibraryW = LoadLibraryW;
    real_CreateProcessW = CreateProcessW;

    big_file.rdbuf()->pubsetbuf(buffer, sizeof(buffer));
    big_file.open("big_file", ifstream::binary);
    output.open("log.txt");
    output << "Starting\n";
    output.flush();
    current_directory = to_lower(fs::current_path().wstring());

    big_file.seekg(-4, ifstream::end);
    auto& stream = big_file;

    uint32_t start_table;
    QUICK_READ(start_table);
    big_file.seekg(start_table, ifstream::beg);

    uint32_t size;
    QUICK_READ(size);

    for (uint32_t i = 0; i < size; ++i) {
        auto file = format::File::deserialize(big_file);
        to_lower_inplace(file.name);
        files.emplace(file.name, file);
    }
}

uint32_t HookerStruct::get_new_handle_position() {
    if (free_positions.empty()) {
        handles.push_back({});
        return handles.size() - 1;
    }
    auto position = free_positions.back();
    free_positions.pop_back();
    return position;
}

void HookerStruct::close_handle(HANDLE handle) {
    auto position = get_handle_position(handle);
    if (position + 1 == handles.size()) {
        handles.pop_back();
    } else {
        free_positions.push_back(position);
    }
}

uint32_t HookerStruct::read_file(uint32_t handle_position, char* buffer, size_t to_read) {
    auto& file = handles[handle_position];

    auto can_read = min(to_read, file.file->size - file.offset);
    big_file.seekg(file.file->offset + file.offset);
    big_file.read(buffer, can_read);
    file.offset += can_read;
    return can_read;
}

uint32_t HookerStruct::get_file_size(uint32_t handle_position) {
    auto& handle = handles[handle_position];
    return handle.file->size;
}

uint32_t HookerStruct::set_file_pointer(uint32_t handle_position, int64_t move, uint32_t how_to_move) {
    auto& handle = handles[handle_position];

    switch (how_to_move) {
    case FILE_BEGIN:
        handle.offset = move;
        break;
    case FILE_CURRENT:
        handle.offset += move;
        break;
    case FILE_END:
        handle.offset = handle.file->size + move;
        break;
    }

    return handle.offset;
}

BOOL APIENTRY DllMain(HMODULE, DWORD reason, LPVOID) {
    switch (reason) {
    case DLL_PROCESS_ATTACH:
        hooker_struct.on_load();
        break;
    case DLL_PROCESS_DETACH:
        hooker_struct.destroy();
        break;
    default:
        break;
    }
    return TRUE;
}